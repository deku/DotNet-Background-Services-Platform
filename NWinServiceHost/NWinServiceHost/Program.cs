﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topshelf;
using System.Diagnostics;

namespace NWinServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            //HostFactor
            var r = HostFactory.Run(config => {
                //config.SetDescription("");
                //config.SetDisplayName("");
                //config.SetServiceName("");
                config.RunAsLocalService();
                config.Service<NServiceHost>();
            });
            //记录r
            Debug.WriteLine(r.ToString());
        }
    }
}
