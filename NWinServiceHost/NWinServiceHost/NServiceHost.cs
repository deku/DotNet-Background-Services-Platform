﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topshelf;
using System.IO;
using CassiniDev;
using NService.Common;

namespace NWinServiceHost
{
    internal class NServiceHost : ServiceControl
    {

        Server svr;

        public bool Start(HostControl hostControl)
        {
            bool result = true;
            try
            {
                //加载管理用的WEB程序
                string webPath = Path.Combine(Environment.CurrentDirectory,"Monitor");
                int port = 52727;
                svr = new Server(port, "/", webPath, false, true);
                svr.Start();
                ServiceManager.Instance.BindAppDomain(svr.HostAppDomain);
                //加载所有需要启动的服务,每个服务使用一个独立的AppDomain
                foreach (var s in ServiceManager.Instance.Services)
                {
                    
                }
                //记录启动失败的服务，不能影响其它服务的启动

                //依次启动设置好需要启动的服务
            }
            catch (Exception e) {
                result = false;
                //记录日志
                //throw;
            }

            return result;
        }

        public bool Stop(HostControl hostControl)
        {
            bool result = true;
            try
            {
                svr.ShutDown();
                svr.Dispose();
                //依次停止正在运行的服务，并释放占用的资源
            }
            catch (Exception e) {
                result = false;
                //记录日志
            }
            return result;
        }
    }
}
