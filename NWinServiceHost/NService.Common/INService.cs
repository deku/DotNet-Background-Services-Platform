﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NService.Common
{
    public interface INService
    {
        /// <summary>
        /// 启动服务
        /// </summary>
        /// <returns></returns>
        INService Start();
        /// <summary>
        /// 停止服务
        /// </summary>
        /// <returns></returns>
        INService Stop();

        /// <summary>
        /// 服务当前的状态
        /// </summary>
        ServiceStatus Status { get; }
    }

    public enum ServiceStatus
    {
        Stop = 0,
        Start = 1,
        Error=2
    }
}
