﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NService.Common
{
    /// <summary>
    /// 描述服务的元数据
    /// </summary>
    [Serializable][XmlType("service")]
    public class ServiceMeta :IDisposable,IComparable<ServiceMeta>
    {

        public ServiceMeta() { }

        /// <summary>
        /// 服务的类型
        /// </summary>
        [XmlAttribute("type")]
        public string TypeName { get; set; }

        /// <summary>
        /// 服务的实例
        /// </summary>
        [XmlIgnore]
        public INService Instance { get; set; }

        /// <summary>
        /// 服务名称
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// 服务需要的内部配置
        /// </summary>
        [XmlElement("params")]
        public object Config { get; set; }


        #region Dispose

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed;
        protected virtual void Dispose(bool disposing) {
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed

                // and unmanaged resources.

                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up

                // unmanaged resources here.

                // If disposing is false,

                // only the following code is executed.

                // Note disposing has been done.

                disposed = true;

            }

        }
        #endregion

        public int CompareTo(ServiceMeta other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
